#            )     (                        (
#    (   ( /( (   )\   (                   )\ )  (   (  (
#  ( )\  )\()))\ ((_) ))\    (   (    (   (()/(  )\  )\))(
#  )(( )(_))/((_) _  /((_)   )\  )\   )\ ) /(_))((_)((_))\
# ((_)_)| |_  (_)| |(_))    ((_)((_) _(_/((_) _| (_) (()(_)
# / _` ||  _| | || |/ -_)  / _|/ _ \| ' \))|  _| | |/ _` |
# \__, | \__| |_||_|\___|  \__|\___/|_||_| |_|   |_|\__, |
#    |_|                                            |___/
#
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List 

mod = "mod4"
mod1 = "mod1"
terminal = "kitty"

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod1], "Tab", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column
    Key([mod, "control"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "control"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "control"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "control"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink. EA
    Key([mod, "shift"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "shift"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "shift"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "shift"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "t", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod], "l", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    Key([mod], "g", lazy.window.toggle_floating(),
        desc="Toggle floating on focused window",),
    Key([mod1], "m", lazy.layout.maximize(),
        desc="Toggle window between minimum and maximum sizes",
    ),

    # Media hotkeys
    Key([], 'XF86AudioRaiseVolume', lazy.spawn('pulseaudio-ctl up 5')),
    Key([], 'XF86AudioLowerVolume', lazy.spawn('pulseaudio-ctl down 5')),
    Key([], 'XF86AudioMute', lazy.spawn('pulseaudio-ctl set 1')),
    Key(
        [], "XF86AudioNext",
        lazy.spawn("playerctl next"),
        desc="Play next audio",
    ),
    Key(
        [], "XF86AudioPlay",
        lazy.spawn("playerctl play-pause"),
        desc="Toggle play/pause audio"
    ),
    Key(
        [], "XF86AudioPrev",
        lazy.spawn("playerctl previous"),
        desc="Play previous audio",
    ),
]

groups = [
    Group("1", label="一"),
    Group("2", label="二"),
    Group("3", label="三"),
    Group("4", label="四"),
    Group("5", label="五"),
    Group("6", label="六"),
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod], "F1", lazy.window.togroup("1", switch_group=True)),
        Key([mod], "F2", lazy.window.togroup("2", switch_group=True)),
        Key([mod], "F3", lazy.window.togroup("3", switch_group=True)),
        Key([mod], "F4", lazy.window.togroup("4", switch_group=True)),
        Key([mod], "F5", lazy.window.togroup("5", switch_group=True)),
        Key([mod], "F6", lazy.window.togroup("6", switch_group=True)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            desc="move focused window to group {}".format(i.name)),
    ])


layouts = [
    layout.Columns(
        border_focus_stack = ['#e06c75', '#be5046'],
        border_width = 3,
        margin = 4,
        ),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(
        border_focus = "#e06c75",
        border_width = 3,
        margin = 4,
        ),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='Source Code Pro Medium',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Image(
                        filename = "~/.config/qtile/icons/logo.png",
                        scale = "False"
                ),
                widget.CurrentLayout(
                    custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                    background="#282c34",
                    foreground="#c678dd",
                    ),
                widget.GroupBox(
                    background="#282c34",
                    foreground="#e5c07b",       
                    ),
                widget.Prompt(
                    background="#282c34",
                    foreground="#c678dd",
                    ),
                widget.WindowName(
                    background="#282c34",
                    foreground="#abb2bf",
                    ),
                widget.Chord(
                    background="#282c34",
                    foreground="#abb2bf",
                    chords_colors={
                        'launch': ("#be5046", "#abb2bf"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Systray(
                    background="#282c34",
                    foreground="#abb2bf",
                    ),
                widget.TextBox(
                    fontsize = 15,
                    text = "墳",
                    background="#282c34",
                    foreground="#98c379",
                ),       
                widget.Volume(
                    background="#282c34",
                    foreground="#abb2bf",
                    ),
    	        widget.TextBox(
    	            font = "Iosevka Nerd Font",
    	            fontsize = 10,
    	            text = " ",
    	            foreground = "#56b6c2",
    	            background = "#282c34",
	            ),
                widget.Clock(
                    background="#282c34",
                    foreground="#abb2bf",
                    format = '%b %d-%Y',
                    ),
				widget.TextBox(
		            font = "Iosevka Nerd Font",
		            fontsize = 15,
		            text = " ",
		            background="#282c34",
                    foreground="#61afef",
		        ),
		        widget.Clock(
		            format = '%I:%M:%S %p',
		            background="#282c34",
                    foreground="#abb2bf",
		        ),
                widget.QuickExit(
                    background="#282c34",
                    foreground="#e06c75",
                    default_text = "[X]"
                ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='copyq'),  # copyq tray
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

### COLORS
# +---------------------------------------------+
# |  Color Name  |         RGB        |   Hex   |
# |--------------+--------------------+---------|
# | Black        | rgb(40, 44, 52)    | #282c34 |
# |--------------+--------------------+---------|
# | White        | rgb(171, 178, 191) | #abb2bf |
# |--------------+--------------------+---------|
# | Light Red    | rgb(224, 108, 117) | #e06c75 |
# |--------------+--------------------+---------|
# | Dark Red     | rgb(190, 80, 70)   | #be5046 |
# |--------------+--------------------+---------|
# | Green        | rgb(152, 195, 121) | #98c379 |
# |--------------+--------------------+---------|
# | Light Yellow | rgb(229, 192, 123) | #e5c07b |
# |--------------+--------------------+---------|
# | Dark Yellow  | rgb(209, 154, 102) | #d19a66 |
# |--------------+--------------------+---------|
# | Blue         | rgb(97, 175, 239)  | #61afef |
# |--------------+--------------------+---------|
# | Magenta      | rgb(198, 120, 221) | #c678dd |
# |--------------+--------------------+---------|
# | Cyan         | rgb(86, 182, 194)  | #56b6c2 |
# |--------------+--------------------+---------|
# | Gutter Grey  | rgb(76, 82, 99)    | #4b5263 |
# |--------------+--------------------+---------|
# | Comment Grey | rgb(92, 99, 112)   | #5c6370 |
# +---------------------------------------------+ 
